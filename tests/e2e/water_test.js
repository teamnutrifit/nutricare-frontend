Feature('water');

// one time test 
Scenario('test registering a new user', ({ I }) => {
    I.amOnPage('/register');
    I.see("Register");
    I.fillField('[data-ruid=inputUsername]', 'testWaterUser');
    I.fillField('[data-ruid=inputEmail]','testWater@user.com');
    I.fillField('[data-ruid=inputPassword]','testWaterPassword');
    I.fillField('[data-ruid=inputFirstname]','testWaterFirstname');
    I.fillField('[data-ruid=inputLastname]','testWaterLastname');
    I.fillField('[data-ruid=inputBirthdate]','14/12/1990');
    I.checkOption('[data-ruid=sexFemale]');
    I.click('[data-ruid=inputRegister]');
    I.see('Log in');
});

// one time test
Scenario('test water entry', ({ I }) => {
   I.amOnPage('/login');
   I.fillField('[data-ruid=login]', 'testWaterUser');
   I.fillField('[data-ruid=password]', 'testWaterPassword');
   I.click('[data-ruid=loginButton]');
   I.see('BIENVENU');
   I.click('[data-ruid=journalNavBar]');
   I.see('JOURNAL');
   I.fillField('[data-ruid=waterQuantityInput]', '3');
   I.fillField('[data-ruid=waterMeasures]', 'dl')
   I.click('[data-ruid=addWaterButton]');
   I.click('[data-ruid=addWaterButton]');
   I.see('24.0%');
});
