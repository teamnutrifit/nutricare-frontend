Feature('food');

// one time test 
Scenario('test registering a new user', ({ I }) => {
    I.amOnPage('/register');
    I.see("Register");
    I.fillField('[data-ruid=inputUsername]', 'testFoodUser');
    I.fillField('[data-ruid=inputEmail]','testFood@user.com');
    I.fillField('[data-ruid=inputPassword]','testFoodPassword');
    I.fillField('[data-ruid=inputFirstname]','testFoodFirstname');
    I.fillField('[data-ruid=inputLastname]','testFoodLastname');
    I.fillField('[data-ruid=inputBirthdate]','14/12/1990');
    I.checkOption('[data-ruid=sexFemale]');
    I.click('[data-ruid=inputRegister]');
    I.see('Log in');
});

// One time test
Scenario('test food entry', ({ I }) => {
   I.amOnPage('/login');
   I.fillField('[data-ruid=login]', 'testFoodUser');
   I.fillField('[data-ruid=password]', 'testFoodPassword');
   I.click('[data-ruid=loginButton]');
   I.see('BIENVENU');
   I.click('[data-ruid=foodNavBar]');
   I.see('ALIMENTS');
   I.click('[data-ruid=buttonAddFood]');
   I.fillField('[data-ruid=foodName]', 'aliment');
   I.fillField('[data-ruid=foodDescription]', 'test e2e aliment ajout');
   I.fillField('[data-ruid=foodCarb]', '4,2');
   I.fillField('[data-ruid=foodSugar]', '4,2');
   I.fillField('[data-ruid=foodFat]', '4,2');
   I.fillField('[data-ruid=foodFatSat]', '4,2');
   I.fillField('[data-ruid=foodProt]', '4,2');
   I.click('[data-ruid=buttonFinalizeFood]');
   I.see('aliment');
});
