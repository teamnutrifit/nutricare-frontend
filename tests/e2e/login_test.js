Feature('login');

// one time test 
Scenario('test registering a new user', ({ I }) => {
    I.amOnPage('/register');
    I.see("Register");
    I.fillField('[data-ruid=inputUsername]', 'testUser');
    I.fillField('[data-ruid=inputEmail]','test@user.com');
    I.fillField('[data-ruid=inputPassword]','testPassword');
    I.fillField('[data-ruid=inputFirstname]','testFirstname');
    I.fillField('[data-ruid=inputLastname]','testLastname');
    I.fillField('[data-ruid=inputBirthdate]','14/12/1996');
    I.checkOption('[data-ruid=sexMale]');
    I.click('[data-ruid=inputRegister]');
    I.see('Log in');
});

Scenario('test login', ({ I }) => {
   I.amOnPage('/login');
   I.fillField('[data-ruid=login]', 'testUser');
   I.fillField('[data-ruid=password]', 'testPassword');
   I.click('[data-ruid=loginButton]')
   I.see('BIENVENU');
});
