// Import the `mount()` method from Vue Test Utils
import { expect } from 'chai'
import { mount } from '@vue/test-utils'

// The component to test
const MessageComponent = {
  template: '<p>{{ msg }}</p>',
  props: ['msg']
}

describe('displays message', () => {
  // mount() returns a wrapped Vue component we can interact with
  const wrapper = mount(MessageComponent, {
    propsData: {
      msg: 'Hello world'
    }
  })
  it('renders correct when passed', () => {
    // Assert the rendered text of the component
    expect(wrapper.text())
      .contain('Hello world')
  })
})
