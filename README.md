![pipeline status](https://gitlab.com/teamnutrifit/nutricare-frontend/badges/master/pipeline.svg)

# Nutricare Front End

To admire the result, you may follow [this link](https://teamnutrifit.gitlab.io/nutricare-frontend).

## Tutorials we followed
[Get datas with axios](https://fr.vuejs.org/v2/cookbook/using-axios-to-consume-apis.html)

[Send datas with axios](https://jasonwatmore.com/post/2020/07/23/vue-axios-http-post-request-examples)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
