import '@/assets/css/main.css';
import vuetify from '@/plugins/vuetify';
import 'bootstrap/dist/css/bootstrap.min.css';
import Vue from 'vue';
import Material from "vue-material";
import "vue-material/dist/vue-material.css";
import App from './App.vue';
import "./assets/scss/material-dashboard.scss";
import store from '@/state/store'
import router from '@/router/router'
import Notifications from 'vue-notification'


Vue.config.productionTip = false;

Vue.use(Material);
Vue.use(Notifications)

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app');

