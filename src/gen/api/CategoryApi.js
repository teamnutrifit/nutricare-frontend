/**
 * Nutricare API
 * This API is used to communicate with the Nutricare Backend Service.
 *
 * The version of the OpenAPI document: 0.0.8
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */


import ApiClient from "../ApiClient";
import CategoryCreationDTO from '../model/CategoryCreationDTO';
import CategoryDTO from '../model/CategoryDTO';
import CategoryModificationDTO from '../model/CategoryModificationDTO';

/**
* Category service.
* @module api/CategoryApi
* @version 0.0.8
*/
export default class CategoryApi {

    /**
    * Constructs a new CategoryApi. 
    * @alias module:api/CategoryApi
    * @class
    * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
    * default to {@link module:ApiClient#instance} if unspecified.
    */
    constructor(apiClient) {
        this.apiClient = apiClient || ApiClient.instance;
    }


    /**
     * Callback function to receive the result of the createCategory operation.
     * @callback module:api/CategoryApi~createCategoryCallback
     * @param {String} error Error message, if any.
     * @param {module:model/CategoryDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Add the recipe category.
     * This private endpoint is used to add a new recipe category in the data base.
     * @param {Object} opts Optional parameters
     * @param {module:model/CategoryCreationDTO} opts.categoryCreationDTO 
     * @param {module:api/CategoryApi~createCategoryCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/CategoryDTO}
     */
    createCategory(opts, callback) {
      opts = opts || {};
      let postBody = opts['categoryCreationDTO'];

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['JWTSecurity'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = CategoryDTO;
      return this.apiClient.callApi(
        '/categories', 'POST',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the getCategories operation.
     * @callback module:api/CategoryApi~getCategoriesCallback
     * @param {String} error Error message, if any.
     * @param {Array.<module:model/CategoryDTO>} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get all recipe categories.
     * This private endpoint is used to get all the recipe categories of a user in the data base.
     * @param {module:api/CategoryApi~getCategoriesCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link Array.<module:model/CategoryDTO>}
     */
    getCategories(callback) {
      let postBody = null;

      let pathParams = {
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['JWTSecurity'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = [CategoryDTO];
      return this.apiClient.callApi(
        '/categories', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the getCategory operation.
     * @callback module:api/CategoryApi~getCategoryCallback
     * @param {String} error Error message, if any.
     * @param {module:model/CategoryDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Get recipe category.
     * This private endpoint is used to get the recipe category with the given id in the data base.
     * @param {String} name 
     * @param {module:api/CategoryApi~getCategoryCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/CategoryDTO}
     */
    getCategory(name, callback) {
      let postBody = null;
      // verify the required parameter 'name' is set
      if (name === undefined || name === null) {
        throw new Error("Missing the required parameter 'name' when calling getCategory");
      }

      let pathParams = {
        'name': name
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['JWTSecurity'];
      let contentTypes = [];
      let accepts = ['application/json'];
      let returnType = CategoryDTO;
      return this.apiClient.callApi(
        '/categories/{name}', 'GET',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }

    /**
     * Callback function to receive the result of the updateCategory operation.
     * @callback module:api/CategoryApi~updateCategoryCallback
     * @param {String} error Error message, if any.
     * @param {module:model/CategoryDTO} data The data returned by the service call.
     * @param {String} response The complete HTTP response.
     */

    /**
     * Add the recipe category.
     * This private endpoint is used to update a recipe category in the data base.
     * @param {String} name 
     * @param {Object} opts Optional parameters
     * @param {module:model/CategoryModificationDTO} opts.categoryModificationDTO 
     * @param {module:api/CategoryApi~updateCategoryCallback} callback The callback function, accepting three arguments: error, data, response
     * data is of type: {@link module:model/CategoryDTO}
     */
    updateCategory(name, opts, callback) {
      opts = opts || {};
      let postBody = opts['categoryModificationDTO'];
      // verify the required parameter 'name' is set
      if (name === undefined || name === null) {
        throw new Error("Missing the required parameter 'name' when calling updateCategory");
      }

      let pathParams = {
        'name': name
      };
      let queryParams = {
      };
      let headerParams = {
      };
      let formParams = {
      };

      let authNames = ['JWTSecurity'];
      let contentTypes = ['application/json'];
      let accepts = ['application/json'];
      let returnType = CategoryDTO;
      return this.apiClient.callApi(
        '/categories/{name}', 'PUT',
        pathParams, queryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType, null, callback
      );
    }


}
