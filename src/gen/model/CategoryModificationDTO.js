/**
 * Nutricare API
 * This API is used to communicate with the Nutricare Backend Service.
 *
 * The version of the OpenAPI document: 0.0.8
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The CategoryModificationDTO model module.
 * @module model/CategoryModificationDTO
 * @version 0.0.8
 */
class CategoryModificationDTO {
    /**
     * Constructs a new <code>CategoryModificationDTO</code>.
     * @alias module:model/CategoryModificationDTO
     */
    constructor() { 
        
        CategoryModificationDTO.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>CategoryModificationDTO</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/CategoryModificationDTO} obj Optional instance to populate.
     * @return {module:model/CategoryModificationDTO} The populated <code>CategoryModificationDTO</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new CategoryModificationDTO();

            if (data.hasOwnProperty('id')) {
                obj['id'] = ApiClient.convertToType(data['id'], 'Number');
            }
            if (data.hasOwnProperty('name')) {
                obj['name'] = ApiClient.convertToType(data['name'], 'String');
            }
        }
        return obj;
    }


}

/**
 * @member {Number} id
 */
CategoryModificationDTO.prototype['id'] = undefined;

/**
 * @member {String} name
 */
CategoryModificationDTO.prototype['name'] = undefined;






export default CategoryModificationDTO;

