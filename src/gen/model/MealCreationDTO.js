/**
 * Nutricare API
 * This API is used to communicate with the Nutricare Backend Service.
 *
 * The version of the OpenAPI document: 0.0.8
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The MealCreationDTO model module.
 * @module model/MealCreationDTO
 * @version 0.0.8
 */
class MealCreationDTO {
    /**
     * Constructs a new <code>MealCreationDTO</code>.
     * @alias module:model/MealCreationDTO
     */
    constructor() { 
        
        MealCreationDTO.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>MealCreationDTO</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/MealCreationDTO} obj Optional instance to populate.
     * @return {module:model/MealCreationDTO} The populated <code>MealCreationDTO</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new MealCreationDTO();

            if (data.hasOwnProperty('name')) {
                obj['name'] = ApiClient.convertToType(data['name'], 'String');
            }
            if (data.hasOwnProperty('dateTimeOfConsumption')) {
                obj['dateTimeOfConsumption'] = ApiClient.convertToType(data['dateTimeOfConsumption'], 'Date');
            }
        }
        return obj;
    }


}

/**
 * @member {String} name
 */
MealCreationDTO.prototype['name'] = undefined;

/**
 * @member {Date} dateTimeOfConsumption
 */
MealCreationDTO.prototype['dateTimeOfConsumption'] = undefined;






export default MealCreationDTO;

