/**
 * Nutricare API
 * This API is used to communicate with the Nutricare Backend Service.
 *
 * The version of the OpenAPI document: 0.0.8
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';

/**
 * The FoodDTO model module.
 * @module model/FoodDTO
 * @version 0.0.8
 */
class FoodDTO {
    /**
     * Constructs a new <code>FoodDTO</code>.
     * @alias module:model/FoodDTO
     */
    constructor() { 
        
        FoodDTO.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>FoodDTO</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/FoodDTO} obj Optional instance to populate.
     * @return {module:model/FoodDTO} The populated <code>FoodDTO</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new FoodDTO();

            if (data.hasOwnProperty('id')) {
                obj['id'] = ApiClient.convertToType(data['id'], 'Number');
            }
            if (data.hasOwnProperty('name')) {
                obj['name'] = ApiClient.convertToType(data['name'], 'String');
            }
            if (data.hasOwnProperty('description')) {
                obj['description'] = ApiClient.convertToType(data['description'], 'String');
            }
            if (data.hasOwnProperty('carbohydrate')) {
                obj['carbohydrate'] = ApiClient.convertToType(data['carbohydrate'], 'Number');
            }
            if (data.hasOwnProperty('sugar')) {
                obj['sugar'] = ApiClient.convertToType(data['sugar'], 'Number');
            }
            if (data.hasOwnProperty('fat')) {
                obj['fat'] = ApiClient.convertToType(data['fat'], 'Number');
            }
            if (data.hasOwnProperty('saturated')) {
                obj['saturated'] = ApiClient.convertToType(data['saturated'], 'Number');
            }
            if (data.hasOwnProperty('protein')) {
                obj['protein'] = ApiClient.convertToType(data['protein'], 'Number');
            }
            if (data.hasOwnProperty('kcal')) {
                obj['kcal'] = ApiClient.convertToType(data['kcal'], 'Number');
            }
        }
        return obj;
    }


}

/**
 * @member {Number} id
 */
FoodDTO.prototype['id'] = undefined;

/**
 * @member {String} name
 */
FoodDTO.prototype['name'] = undefined;

/**
 * @member {String} description
 */
FoodDTO.prototype['description'] = undefined;

/**
 * @member {Number} carbohydrate
 */
FoodDTO.prototype['carbohydrate'] = undefined;

/**
 * @member {Number} sugar
 */
FoodDTO.prototype['sugar'] = undefined;

/**
 * @member {Number} fat
 */
FoodDTO.prototype['fat'] = undefined;

/**
 * @member {Number} saturated
 */
FoodDTO.prototype['saturated'] = undefined;

/**
 * @member {Number} protein
 */
FoodDTO.prototype['protein'] = undefined;

/**
 * @member {Number} kcal
 */
FoodDTO.prototype['kcal'] = undefined;






export default FoodDTO;

