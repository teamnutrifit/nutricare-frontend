/**
 * Nutricare API
 * This API is used to communicate with the Nutricare Backend Service.
 *
 * The version of the OpenAPI document: 0.0.8
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import FoodInDTO from './FoodInDTO';

/**
 * The MealDTO model module.
 * @module model/MealDTO
 * @version 0.0.8
 */
class MealDTO {
    /**
     * Constructs a new <code>MealDTO</code>.
     * @alias module:model/MealDTO
     */
    constructor() { 
        
        MealDTO.initialize(this);
    }

    /**
     * Initializes the fields of this object.
     * This method is used by the constructors of any subclasses, in order to implement multiple inheritance (mix-ins).
     * Only for internal use.
     */
    static initialize(obj) { 
    }

    /**
     * Constructs a <code>MealDTO</code> from a plain JavaScript object, optionally creating a new instance.
     * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
     * @param {Object} data The plain JavaScript object bearing properties of interest.
     * @param {module:model/MealDTO} obj Optional instance to populate.
     * @return {module:model/MealDTO} The populated <code>MealDTO</code> instance.
     */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new MealDTO();

            if (data.hasOwnProperty('id')) {
                obj['id'] = ApiClient.convertToType(data['id'], 'Number');
            }
            if (data.hasOwnProperty('name')) {
                obj['name'] = ApiClient.convertToType(data['name'], 'String');
            }
            if (data.hasOwnProperty('dateTimeOfConsumption')) {
                obj['dateTimeOfConsumption'] = ApiClient.convertToType(data['dateTimeOfConsumption'], 'Date');
            }
            if (data.hasOwnProperty('foods')) {
                obj['foods'] = ApiClient.convertToType(data['foods'], [FoodInDTO]);
            }
        }
        return obj;
    }


}

/**
 * @member {Number} id
 */
MealDTO.prototype['id'] = undefined;

/**
 * @member {String} name
 */
MealDTO.prototype['name'] = undefined;

/**
 * @member {Date} dateTimeOfConsumption
 */
MealDTO.prototype['dateTimeOfConsumption'] = undefined;

/**
 * @member {Array.<module:model/FoodInDTO>} foods
 */
MealDTO.prototype['foods'] = undefined;






export default MealDTO;

