import Vue from "vue"
import VueRouter from "vue-router"
import store from '@/state/store'

Vue.use(VueRouter);

const routes = [
  {
    path: "/aboutus",
    name: "aboutus",
    component: () => import("../views/AboutUs.vue")
  },
  {
    path: "/tutorial",
    name: "tutorial",
    component: () => import("../views/Tutorial.vue")
  },
  {
    path: "/",
    name: "landing",
    component: () => import("../views/Landing.vue")
  },
  {
    path: "/home",
    name: "home",
    component: () => import("../views/Home.vue"),
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "/profile",
    name: "userprofile",
    component: () => import("../views/UserProfile.vue"),
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "/categories",
    name: "categories",
    component: () => import("../views/recipes/Categories.vue"),
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "/category/:name",
    name: "category",
    component: () => import("../views/recipes/Category.vue"),
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "/recipe/:name",
    name: "recipe",
    component: () => import("../views/recipes/Recipe.vue"),
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "/createRecipe",
    name: "createRecipe",
    component: () => import("../views/recipes/CreateRecipe.vue"),
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "/recipes",
    name: "recipes",
    component: () => import("../views/recipes/Recipes.vue"),
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "/journal",
    name: "journal",
    component: () => import("../views/Journal.vue"),
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "/food",
    name: "food",
    component: () => import("../views/Food.vue"),
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "/messages",
    name: "messages",
    component: () => import("../views/message/Messages.vue"),
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: "/administration",
    name: "administration",
    component: () => import("../views/Administration.vue"),
    meta: {
      requiresAuth: true,
      isAdmin: true
    }
  },
  {
    path: "/register",
    name: "register",
    component: () => import("../views/connexion/Register.vue")
  },
  {
    path: "/login",
    name: "login",
    component: () => import("../views/connexion/Login.vue")
  },
  {
    path: "/forgot-password",
    name: "forgot-password",
    component: () => import("../views/connexion/ForgotPassword.vue")
  },
  {
    path: "*",
    name: "NotFound",
    component: () => import("../views/NotFound.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

/**
 * Redirections for auth and admin
 * Source:
 * https://router.vuejs.org/guide/advanced/navigation-guards.html#per-route-guard
 */
router.beforeEach((to, from, next) => {
  // Auth required not Public
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // Not logged in
    if (!store.getters.isLoggedIn) {
      next({
        name: 'login',
        params: { nextUrl: to.fullPath }
      });
    }
    // Logged in
    else {
      // Admin is required
      if (to.matched.some(record => record.meta.isAdmin)) {
        // isAdmin
        if (store.getters.isAdmin) {
          next();
        }
        //is Not Admin
        else {
          next({ name: 'NotFound' });
        }
      }
      // Admin is not required
      else {
        next();
      }
    }
  }
  // Public
  else {
    next();
  }
});

export default router;
