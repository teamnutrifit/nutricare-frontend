
function roundZeroDecimal(number) {
  return Math.round(number);
};

function roundOneDecimal(number) {
  return Math.round(number * 10) / 10;
};

function computeFoodValuesRounded(value, quantity) {
  return roundOneDecimal(value / 100 * quantity);
};

export default {
  formatDate(value) {
    if (value) {
      var date = new Date(value);
      var dd = String(date.getDate()).padStart(2, "0");
      var mm = String(date.getMonth() + 1).padStart(2, "0");
      var yyyy = date.getFullYear();

      date = dd + "." + mm + "." + yyyy;
      return date;
    }
  },
  formatDateForBackend(value) {
    if (value) {
      var date = new Date(value);
      var dd = String(date.getDate()).padStart(2, "0");
      var mm = String(date.getMonth() + 1).padStart(2, "0");
      var yyyy = date.getFullYear();

      date = yyyy + "-" + mm + "-" + dd;
      return date;
    }
  },
  createDatetime(date, time) {
    let t = time.split(":");
    let datetime = new Date(date);
    datetime.setHours(t[0]);
    datetime.setMinutes(t[1]);
    return datetime;
  },
  getTime(datetime) {
    // console.log(datetime.getTimezoneOffset());
    const offset = datetime.getTimezoneOffset() / 60;

    let time = "";
    time += datetime.getHours() < 10 ? "0" + (datetime.getHours() - offset) : (datetime.getHours() - offset);
    time += ":";
    time += datetime.getMinutes() < 10 ? "0" + datetime.getMinutes() : datetime.getMinutes()
    return time;
  },
  now() {
    return new Date();
  },
  nowTime() {
    let time = "";
    const date = new Date();
    time += date.getHours();
    time += ":";
    time += date.getMinutes();
    return time;
  },
  generateLastNDaysBackendDateArray(n) {
    let arr = [];
    for (let i = n - 1; i >= 0; i--) {
      let d = this.now();
      d.setDate(d.getDate() - i);
      arr.push(this.formatDateForBackend(d));
    }
    return arr;
  },

  methodComputeFoodsValues(foods) {
    let arr = [];
    for (let i = 0; i < foods.length; i++) {
      const foodIn = foods[i];
      arr.push({
        quantity: foodIn.quantity,
        name: foodIn.food.name,
        description: foodIn.food.description,
        carbohydrate: computeFoodValuesRounded(
          foodIn.food.carbohydrate,
          foodIn.quantity
        ),
        sugar: computeFoodValuesRounded(
          foodIn.food.sugar,
          foodIn.quantity
        ),
        fat: computeFoodValuesRounded(foodIn.food.fat, foodIn.quantity),
        saturated: computeFoodValuesRounded(
          foodIn.food.saturated,
          foodIn.quantity
        ),
        protein: computeFoodValuesRounded(
          foodIn.food.protein,
          foodIn.quantity
        ),
        kcal: computeFoodValuesRounded(
          foodIn.food.kcal,
          foodIn.quantity
        ),
        density: this.computeCaloricDensity(foodIn.food.kcal)
      });
    }
    return arr;
  },
  methodComputeFoodsValuesTotal(foods) {
    const arr = this.methodComputeFoodsValues(foods);

    let quantity = 0.0;
    let carbohydrate = 0.0;
    let sugar = 0.0;
    let fat = 0.0;
    let saturated = 0.0;
    let protein = 0.0;
    let kcal = 0.0;
    let density = 0.0;

    for (let i = 0; i < arr.length; i++) {
      const e = arr[i];
      quantity += e.quantity;
      carbohydrate += e.carbohydrate;
      sugar += e.sugar;
      fat += e.fat;
      saturated += e.saturated;
      protein += e.protein;
      kcal += e.kcal;
      density += e.density * e.quantity;
    }

    density = roundZeroDecimal(density / quantity);

    return {
      quantity,
      carbohydrate,
      sugar,
      fat,
      saturated,
      protein,
      kcal,
      density
    };
  },
  /**
   * Computes the caloric density for 100g of a given food.
   * The caloric density is a percentage.
   * @param {*} kcal 
   */
  computeCaloricDensity(kcal) {
    return roundZeroDecimal(100.0 / 900.0 * kcal);
  }

};