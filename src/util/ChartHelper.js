import Notifications from "./Notifications";
import Utils from './Utils';

const notificationsEnabled = true;

function roundOneDecimal(number) {
  return Math.round(number * 10) / 10;
}

export default {
  generateChartDataForMeal(meal) {
    
    if (isNaN(meal.density)) {
      meal.density = 0;
    }
    // Returns the data object (or chartData prop)
    return {
      datasets: [
        {
          data: [meal.density, 100 - meal.density],
          labels: this.generateKcalLabelsArray(),
          //densité > légereté
          backgroundColor: ["#ffdb5a", "#d1d1d1"],
        },
        {
          data: this.generateMacroDataArray(
            meal.carbohydrate,
            meal.sugar,
            meal.fat,
            meal.saturated,
            meal.protein
          ),
          labels: this.generateMacroLabelsArray(),
          //glucides > sucres > lipides > saturés > protéines
          backgroundColor: [
            "#e6535f",
            "#fdb6bc",
            "#31b173",
            "#8af0be",
            "#6a52e6",
          ],
        },
      ],
    };
  },
  generateChartData(food) {
    // Returns the data object (or chartData prop)
    return {
      datasets: [
        {
          data: this.generateKcalDataArray(
            food.carbohydrate,
            food.fat,
            food.protein
          ),
          labels: this.generateKcalLabelsArray(),
          //densité > légereté
          backgroundColor: ["#ffdb5a", "#d1d1d1"],
        },
        {
          data: this.generateMacroDataArray(
            food.carbohydrate,
            food.sugar,
            food.fat,
            food.saturated,
            food.protein
          ),
          labels: this.generateMacroLabelsArray(),
          //glucides > sucres > lipides > saturés > protéines
          backgroundColor: [
            "#e6535f",
            "#fdb6bc",
            "#31b173",
            "#8af0be",
            "#6a52e6",
          ],
        },
      ],
    };
  },
  generateChartOptionsForMeal() {
    // Returns the options object (or options prop)
    return {
      legend: {
        display: false,
      },
      tooltips: {
        callbacks: {
          label: function (tooltipItem, data) {
            // console.log(tooltipItem);
            // console.log(data);
            let label = "";

            // Dataset 1
            if (tooltipItem.datasetIndex === 0) {
              label += data.datasets[tooltipItem.datasetIndex].labels[tooltipItem.index];

              label += ": ";

              const percentage = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];

              label += percentage;
              label += "%";
            }
            // Dataset 2
            else {
              label += data.datasets[tooltipItem.datasetIndex].labels[tooltipItem.index];

              label += ": ";

              label += roundOneDecimal(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);

              label += "g";
            }

            return label;
          },
        },
      },
    };
  },
  
  generateChartOptions() {
    // Returns the options object (or options prop)
    return {
      legend: {
        display: false,
      },
      tooltips: {
        callbacks: {
          label: function (tooltipItem, data) {
            // console.log(tooltipItem);
            // console.log(data);
            let label = "";

            // Dataset 1
            if (tooltipItem.datasetIndex === 0) {
              label += data.datasets[tooltipItem.datasetIndex].labels[tooltipItem.index];

              label += ": ";

              const kcal = roundOneDecimal(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);
              const percentage = Math.round((kcal / 900.0) * 100);

              label += percentage;
              label += "%";
            }
            // Dataset 2
            else {
              label += data.datasets[tooltipItem.datasetIndex].labels[tooltipItem.index];

              label += ": ";

              label += roundOneDecimal(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);

              label += "g";
            }

            return label;
          },
        },
      },
    };
  },

  // Macro
  generateMacroDataArray(carbohydrate, sugar, fat, saturated, protein) {
    return [carbohydrate - sugar, sugar, fat - saturated, saturated, protein];
  },
  generateMacroLabelsArray() {
    return ["Glucides", "Sucres", "Lipides", " Gras Saturés", "Protéines"];
  },
  generateMacroBackgroundColorArray() {
    return ["#2B74A1", "#4E9ED0", "#039D39", "#04C849", "#d13e0d"];
  },

  // Kcal
  generateKcalDataArray(carbohydrate, fat, protein) {
    return [this.computeKcal(carbohydrate, fat, protein), 900 - this.computeKcal(carbohydrate, fat, protein)];
  },
  generateKcalLabelsArray() {
    return ["Densité calorique", "Légèreté calorique"];
  },
  generateKcalBackgroundColorArray() {
    return ["#fcba03", "#9E9E97"];
  },

  // Utility
  computeKcal(carbohydrate, fat, protein) {
    const kcal = 4 * carbohydrate + 9 * fat + 4 * protein;
    const rounded = Math.round(kcal * 10) / 10;
    return isNaN(rounded) ? 0 : rounded;
  },
  validate(carbohydrate, sugar, fat, saturated, protein) {
    let valid = true;

    if (isNaN(carbohydrate) || isNaN(sugar) || isNaN(fat) || isNaN(saturated) || isNaN(protein)) {
      Notifications.warn("Mauvaise Valeur", "La valeur doit être un nombre !")
      return false;
    }

    if (carbohydrate == 0 && sugar == 0 && fat == 0 && saturated == 0 && protein == 0) {
      Notifications.warn("Aucune Valeur", "Veuillez entrer des valeurs !")
      valid = false;
    }

    // Sugar cannot be bigger than carbohydrate
    if (sugar > carbohydrate) {
      valid = false;
      Notifications.warn("Mauvaise Valeur", "Le sucre doit être inclut dans les glucides.")
    }

    // Saturated cannot be bigger than fat
    if (saturated > fat) {
      valid = false;
      Notifications.warn("Mauvaise Valeur", "Les saturés doivent être inclus dans les lipides.")
    }

    // Total weight cannot exceed 100g
    if (carbohydrate + fat + protein > 100) {
      valid = false;
      Notifications.warn("Mauvaise Valeur", "La somme des glucides, lipides et protéines doit être inférieure ou égale à 100g.")
    }

    return valid;
  },

};

