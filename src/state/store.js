import Vue from 'vue';
import Vuex from 'vuex';
import connexion from './modules/connexion'
import profile from './modules/profile'
import messaging from './modules/messaging'
import recipes from './modules/recipes/recipes'
import recipe from './modules/recipes/recipe'
import weight from './modules/weight'
import healthInformation from './modules/healthInformation'
import categories from './modules/recipes/categories'
import category from './modules/recipes/category'
import food from './modules/food'
import journal from './modules/journal'


Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        connexion,
        profile,
        messaging,
        recipes,
        recipe,
        healthInformation,
        weight,
        categories,
        category,
        food,
        journal,
    }
});

export default store;
