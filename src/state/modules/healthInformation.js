import States from '@/state/States'
import HealthInformationApi from '@/gen/api/HealthInformationApi'
import ApiClient from '../../gen/ApiClient';
import StateHelper from '@/state/StateHelper';
import Notifications from '../../util/Notifications';

const healthInformation = {
    state: () => ({
        healthInformation: {
            state: States.INIT,
            message: "message",
            data: {
                "id": 0,
                "goal": "goal string",
                "activityLevel": "string activityLevel",
                "height": 0,
                "weights": [
                    {
                        "id": 0,
                        "weight": 0,
                        "weightDate": "2020-12-30"
                    }
                ]
            }
        },
    }),
    getters: {
        getHealthInformation(state) {
            return state.healthInformation;
        },
        getHealthInformationCurrentWeight(state) {
            if (state.healthInformation.data.weights.length) {
                return state.healthInformation.data.weights[state.healthInformation.data.weights.length - 1].weight;                
            } else {
                return null;
            }
        },
    },
    actions: {
        /**
         *
         * @param {*} { commit } the context contains a function called commit used to commit mutations.
         * @param {*} data additional data object for the action
         */
        actionGetHealthInformation({ commit }) {
            // First commit the waiting mutation
            commit('mutationHealthInformationWaiting', {});
            const api = new HealthInformationApi();

            api.getUserHealth((e, d, r) => {
                if (e) {
                    commit('mutationHealthInformationError', { ...e.response.body });
                    Notifications.error("Erreur de Chargement", data.message);
                } else {
                    // console.log(d)
                    commit('mutationHealthInformationSuccess', d);
                    Notifications.debug("Health Informations", "Loaded Successfully.");
                }
            });
        },
        actionUpdateHealthInfo({ commit }, healthInfoModificationDTO) {
            // First commit the waiting mutation
            commit('mutationHealthInformationWaiting', {});
            const api = new HealthInformationApi();

            api.updateHealthInfo({ healthInfoModificationDTO }, (e, d, r) => {
                if (e) {
                    commit('mutationHealthInformationError', { ...e.response.body });
                    Notifications.error("Erreur de Modification", data.message);
                } else {
                    commit('mutationHealthInformationSuccess', d);
                    Notifications.success("Modification Réussie", "Les informations ont été mises à jour.");
                }
            });
        },
    },
    mutations: {
        // Here we REUSED the same mutations for GET and UPDATE ! 
        // That's why Notifications placement is different.
        mutationHealthInformationWaiting(state) {
            state.healthInformation.state = States.WAITING;
        },
        mutationHealthInformationSuccess(state, healthInformation) {
            state.healthInformation.state = States.SUCCESS;
            state.healthInformation.data = { ...healthInformation };
        },
        mutationHealthInformationError(state, data) {
            state.healthInformation.state = States.ERROR;
            state.healthInformation.message = data.message;
        },
    },
}

export default healthInformation;
