import States from '@/state/States'
import UserApi from '@/gen/api/UserApi'
import Notifications from '../../util/Notifications';
import ImageApi from '@/gen/api/ImageApi';
import StateHelper from '@/state/StateHelper';

const profile = {
  state: () => ({
    profile: {
      state: States.INIT,
      message: "string",
      data: {
        "idUser": 0,
        "username": "",
        "firstname": "",
        "lastname": "",
        "birthdate": "2021-01-09",
        "email": "",
        "password": "", // TODO delete from backend and from here !!!
        "sex": "",
        "roles": [
          {
            "id": 1,
            "name": "USER"
          }
        ],
        image: {
          id: 0,
          image: "",
        },
      }
    }
  }),
  getters: {
    getProfile(state) {
      return state.profile;
    },
    getProfileImage(state) {

      const defaultImage = "data:application/octet-stream;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAMAAACahl6sAAAAG1BMVEXMzMwAAACysrJ/f39MTEwZGRlmZmaZmZkzMzOjeu9dAAAB40lEQVR42u3WSZLiQBBEUWWkcrj/ibsFtDwx4caiOky1+G9V5QbhhCbYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOC3i4hPsUt/Z2HdWzm0HmscvX1JjZsKRytSlXel3aTGTYW1rOr7gNY0wqRGYqGfu9fYtqiPV8czHY83HvFj1DCpcUvh0MV3DK7LnzqC06TObYV67f4sW981j+NmUuPmwjjf15/nUXXdpBf72CT29EI/99z+Epv0zb5eyNHKTCz0xnmmWylN+fGfSa8TtEnoCk8p9Pp58rS8DoxLr8/WoT16XqGn2yleR0rXSzGp2UR75BR6ekaYuWFSs4n2SCv0jupxNvTLhI+p2aRqj6RCb9f+PzlA+g3Sswv9jdfix3O1Sc8u/D5WE3RPulQkmp5daYV+rKr13NMD3KRuD43LL/RjtbxaTOr22DUwp9Dfdm2sgWr08DCpaI+u75P0Qompy/Xy20HjXCraQ98naYWmesaHR7w6mkntHtokr1DUN+Mfpa9zX49h1aR2D22SVihaejGXu6+0OVvR6XXp+yJ9/cgtcgu/z912ZRpgUonWN6ktsgu/z93GfCVjE5Ma9xZKjL9c6uUUAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/ZH+moEHPt3DitAAAAAElFTkSuQmCC";

      const makeBlob = function (dataUrl) {
        // console.log("data url : ");
        // console.log(dataUrl)
        if (dataUrl === null || dataUrl === undefined || dataUrl === "" || dataUrl === "EMPTY") {
          dataUrl = defaultImage;
        }

        var arr = dataUrl.split(","),
          mime = arr[0].match(/:(.*?);/)[1],
          bstr = atob(arr[1]),
          n = bstr.length,
          u8arr = new Uint8Array(n);
        while (n--) {
          u8arr[n] = bstr.charCodeAt(n);
        }
        return new Blob([u8arr], { type: mime });
      };

      let blob;

      try {
        blob = makeBlob(state.profile.data.image.image)
      } catch (error) {
        blob = makeBlob(defaultImage);
      }

      return URL.createObjectURL(blob);
    }
  },
  actions: {
    /**
     *
     * @param {*} { commit } the context contains a function called commit used to commit mutations.
     * @param {*} data additional data object for the action
     */
    actionGetUser({ commit }, username) {
      // First commit the waiting mutation
      commit('mutationGetUserWaiting', {});
      const api = new UserApi();

      api.getUser(username, (e, d, r) => {
        if (e) {
          StateHelper.simpleErrorManagement(e, 'mutationGetUserError', commit);
        } else {
          commit('mutationGetUserSuccess', { ...d });
        }
      });
    },
    actionUpdateUser({ commit }, userModificationDTO) {
      // First commit the waiting mutation
      commit('mutationUpdateUserWaiting', {});
      const api = new UserApi();

      api.updateUser(userModificationDTO.username, { userModificationDTO }, (e, d, r) => {
        if (e) {
          StateHelper.simpleErrorManagement(e, 'mutationUpdateUserError', commit);
        } else {
          commit('mutationUpdateUserSuccess', { ...d });
        }
      });
    },

    actionGetUserImage({ commit }) {
      // First commit the waiting mutation
      commit('mutationGetUserImageWaiting', {});
      const api = new ImageApi();
      api.getCurrentUserImage((e, d, r) => {
        if (e) {
          StateHelper.simpleErrorManagement(e, 'mutationGetUserImageError', commit);
        } else {
          commit('mutationGetUserImageSuccess', { ...d });
        }
      });
    },

    actionUpdateUserImage({ commit }, image) {
      // First commit the waiting mutation
      commit('mutationUpdateUserImageWaiting', {});
      const api = new ImageApi();

      //console.log(image);
      api.updateImageUser({ body: image }, (e, d, r) => {
        if (e) {
          StateHelper.simpleErrorManagement(e, 'mutationUpdateUserImageError', commit);
        } else {
          commit('mutationUpdateUserImageSuccess', { ...d });
        }
      })
    }
  },
  mutations: {
    /**
     *
     * @param {*} state the application state in Vuex store to modify.
     * @param {*} data Additional data to update the state accordingly.
     */

    // Get User
    mutationGetUserWaiting(state, data) {
      state.profile.state = States.WAITING;
    },
    mutationGetUserSuccess(state, data) {
      state.profile.state = States.SUCCESS;
      state.profile.data = { ...data };
      Notifications.debug("Profile", "Loaded Successfully.");
    },
    mutationGetUserError(state, data) {
      state.profile.state = States.ERROR;
      state.profile.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Update User
    mutationUpdateUserWaiting(state, data) {
      state.profile.state = States.WAITING;
    },
    mutationUpdateUserSuccess(state, data) {
      state.profile.state = States.SUCCESS;
      state.profile.data = { ...data };
      Notifications.success("Modification Réussie", "Votre profil a été mis à jour.");
    },
    mutationUpdateUserError(state, data) {
      state.profile.state = States.ERROR;
      state.profile.message = data.message;
      Notifications.error("Erreur de Modification", data.message);
    },

    // Get image
    mutationGetUserImageWaiting(state, data) {
      state.profile.state = States.WAITING;
    },
    mutationGetUserImageSuccess(state, data) {
      state.profile.state = States.SUCCESS;
      state.profile.data.image = { ...data };
      Notifications.debug("Profile Image", "Loaded Successfully.");
    },
    mutationGetUserImageError(state, data) {
      state.profile.state = States.ERROR;
      state.profile.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Update image
    mutationUpdateUserImageWaiting(state, data) {
      state.profile.state = States.WAITING;
    },
    mutationUpdateUserImageSuccess(state, data) {
      state.profile.state = States.SUCCESS;
      state.profile.data.image = { ...data };
      Notifications.success("Modification Réussie", "Votre image de profil a été mise à jour.");
    },
    mutationUpdateUserImageError(state, data) {
      state.profile.state = States.ERROR;
      state.profile.message = data.message;
      Notifications.error("Erreur de Modification", data.message);
    }
  },
}

export default profile;
