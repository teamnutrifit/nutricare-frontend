import States from '@/state/States'
import RecipeApi from '@/gen/api/RecipeApi'
import RecipeCreationDTO from '../../../gen/model/RecipeCreationDTO';
import store from '@/state/store'
import RecipeDTO from '../../../gen/model/RecipeDTO';
import Notifications from '../../../util/Notifications';
import StateHelper from '@/state/StateHelper';
import router from '@/router/router';

const recipes = {
    state: () => ({
        recipes: {
            state: States.INIT,
            message: "message",
            data: []
        },
    }),
    getters: {
        getRecipes(state) {
            return state.recipes;
        },
    },
    actions: {
        /**
         *
         * @param {*} { commit } the context contains a function called commit used to commit mutations.
         * @param {*} data additional data object for the action
         */
        actionGetRecipes({ commit }) {
            // First commit the waiting mutation
            commit('mutationGetRecipesWaiting', {});
            const api = new RecipeApi();

            api.getRecipes((e, d, r) => {
                if (e) {
                    StateHelper.simpleErrorManagement(e, 'mutationGetRecipesError', commit);
                } else {
                    commit('mutationGetRecipesSuccess', d);
                }
            });
        },
        actionGetRecipesOfCategory({ commit }, category) {
            // First commit the waiting mutation
            commit('mutationGetRecipesWaiting', {});
            const api = new RecipeApi();

            api.getRecipesOfCategory(category, (e, d, r) => {
                if (e) {
                    StateHelper.simpleErrorManagement(e, 'mutationGetRecipesError', commit);
                } else {
                    commit('mutationGetRecipesSuccess', d);
                }
            });
        },
        actionPostRecipe({ commit }, recipeCreationDTO) {
            commit('mutationPostRecipeWaiting');
            const api = new RecipeApi();

            api.createRecipe({ recipeCreationDTO } , (e, d, r) => {
                if (e) {
                    StateHelper.simpleErrorManagement(e, 'mutationPostRecipeError', commit);
                } else {
                    commit('mutationPostRecipeSuccess', d);
                }
            });
        },
    },
    mutations: {
        /**
         *
         * @param {*} state the application state in Vuex store to modify.
         * @param {*} data Additional data to update the state accordingly.
         */
        // Get
        mutationGetRecipesWaiting(state) {
            state.recipes.state = States.WAITING;
        },
        mutationGetRecipesSuccess(state, recipes) {
            state.recipes.state = States.SUCCESS;
            state.recipes.data = [...recipes];
        },
        mutationGetRecipesError(state, data) {
            state.recipes.state = States.ERROR;
            state.recipes.message = data.message;
            Notifications.error("Erreur de Chargement", data.message);
        },

        // Post
        mutationPostRecipeWaiting(state) {
            state.recipes.state = States.WAITING;
        },
        mutationPostRecipeSuccess(state, data) {
            router.push('/recipe/' + data.name);
            Notifications.success("Ajout Réussi", "La recette a été crée.");
        },
        mutationPostRecipeError(state, data) {
            state.recipes.state = States.ERROR;
            state.recipes.message = data.message;
            Notifications.error("Erreur d'Ajout", data.message);
        },

    },
}

export default recipes;
