import States from '@/state/States'
import CategoryApi from '@/gen/api/CategoryApi'
import NutritionApi from '@/gen/api/NutritionApi'

import CategoryCreationDTO from '../../../gen/model/CategoryCreationDTO';
import store from '@/state/store'
import CategoryDTO from '../../../gen/model/CategoryDTO';
import Notifications from '../../../util/Notifications';
import StateHelper from '@/state/StateHelper';

const category = {
    state: () => ({
        category: {
            state: States.INIT,
            message: "message",
            data: {
                name: "category name string",
                recipes: []
            }
        },
    }),
    getters: {
        getCategory(state) {
            return state.category;
        },
    },
    actions: {
        /**
         *
         * @param {*} { commit } the context contains a function called commit used to commit mutations.
         * @param {*} data additional data object for the action
         */
        actionGetCategory({ commit }, categoryName) {
            // First commit the waiting mutation
            commit('mutationGetCategoryWaiting', {});
            const api = new CategoryApi();

            api.getCategory(categoryName, (e, d, r) => {
                if (e) {
                    StateHelper.simpleErrorManagement(e, 'mutationGetCategoryError', commit);
                } else {
                    commit('mutationGetCategorySuccess', d);
                }
            });
        },
        actionGetCategoryRecipes({ commit }, categoryName) {//TODO finish and rename mutation names
            // First commit the waiting mutation
            commit('mutationGetCategoryWaiting', {});
            const api = new NutritionApi();

            api.getRecipesOfCategory(categoryName, (e, d, r) => {
                if (e) {
                    StateHelper.simpleErrorManagement(e, 'mutationGetCategoryError', commit);
                } else {
                    commit('mutationGetCategorySuccess', d);
                }
            });
        },
    },
    mutations: {
        /**
         *
         * @param {*} state the application state in Vuex store to modify.
         * @param {*} data Additional data to update the state accordingly.
         */
        // Get
        mutationGetCategoryWaiting(state) {
            state.category.state = States.WAITING;
        },
        mutationGetCategorySuccess(state, category) {
            state.category.state = States.SUCCESS;
            state.category.data = { ...category };
        },
        mutationGetCategoryError(state, data) {
            state.category.state = States.ERROR;
            state.category.message = data.message;
            Notifications.error("Category Loading Error", data.message);
        },
    },
}

export default category;