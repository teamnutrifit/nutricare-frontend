import States from '@/state/States'
import RecipeApi from '@/gen/api/RecipeApi'
import RecipeCreationDTO from '../../../gen/model/RecipeCreationDTO';
import store from '@/state/store'
import RecipeDTO from '../../../gen/model/RecipeDTO';
import Notifications from '../../../util/Notifications';
import StateHelper from '@/state/StateHelper';

const recipe = {
  state: () => ({
    recipe: {
      state: States.INIT,
      message: "message",
      data: {
        name: "",
        author: "",
        category: "",
        foods: [],
        instructions: [],
        id: "",
      }
    },
  }),
  getters: {
    getRecipe(state) {
      return state.recipe;
    },
  },
  actions: {
    /**
     *
     * @param {*} { commit } the context contains a function called commit used to commit mutations.
     * @param {*} name additional data object for the action
     */
    actionGetRecipe({ commit }, name) {
      // First commit the waiting mutation
      commit('mutationGetRecipeWaiting', {});
      const api = new RecipeApi();

      api.getRecipe(name, (e, d, r) => {
        if (e) {
          StateHelper.simpleErrorManagement(e, 'mutationGetRecipeError', commit);
        } else {
          commit('mutationGetRecipeSuccess', d);
        }
      });
    },
    actionPutRecipe({ commit }, { name, recipeDTO } ) {
      commit('mutationPutRecipeWaiting');
      const api = new RecipeApi();

      api.updateRecipe(name, { recipeDTO }, (e, d, r) => {
        if (e) {
          StateHelper.simpleErrorManagement(e, 'mutationPutRecipeError', commit);
        } else {
          commit('mutationPutRecipeSuccess', d);
        }
      });
    },
  },
  mutations: {
    /**
     *
     * @param {*} state the application state in Vuex store to modify.
     * @param {*} data Additional data to update the state accordingly.
     */
    // Get
    mutationGetRecipeWaiting(state) {
      state.recipe.state = States.WAITING;
    },
    mutationGetRecipeSuccess(state, recipe) {
      state.recipe.state = States.SUCCESS;
      state.recipe.data = {...recipe};
    },
    mutationGetRecipeError(state, data) {
      state.recipe.state = States.ERROR;
      state.recipe.message = data.message;
      Notifications.error("Recipe Loading Error", data.message);
    },

    // Put
    mutationPutRecipeWaiting(state) {
      state.recipe.state = States.WAITING;
    },
    mutationPutRecipeSuccess(state) {
      // Just refresh all the recipe form the server
      store.dispatch('actionGetRecipes');
    },
    mutationPutRecipeError(state, data) {
      state.recipe.state = States.ERROR;
      state.recipe.message = data.message;
      Notifications.error("Recipe Update Error", data.message);
    },
  },
}

export default recipe;
