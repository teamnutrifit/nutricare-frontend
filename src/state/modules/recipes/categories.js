import States from '@/state/States'
import CategoryApi from '@/gen/api/CategoryApi'
import CategoryCreationDTO from '../../../gen/model/CategoryCreationDTO';
import store from '@/state/store'
import CategoryModificationDTO from '../../../gen/model/CategoryModificationDTO';
import Notifications from '../../../util/Notifications';
import StateHelper from '@/state/StateHelper';

const categories = {
    state: () => ({
        categories: {
            state: States.INIT,
            message: "message",
            data: []
        },
    }),
    getters: {
        getCategories(state) {
            return state.categories;
        },
    },
    actions: {
        /**
         *
         * @param {*} { commit } the context contains a function called commit used to commit mutations.
         * @param {*} data additional data object for the action
         */
        actionGetCategories({ commit }) {
            // First commit the waiting mutation
            commit('mutationGetCategoriesWaiting', {});
            const api = new CategoryApi();

            api.getCategories((e, d, r) => {
                if (e) {
                    StateHelper.simpleErrorManagement(e, 'mutationGetCategoriesError', commit);
                } else {
                    commit('mutationGetCategoriesSuccess', d);
                }
            });
        },
        actionPostCategory({ commit }, name) {
            commit('mutationPostCategoryWaiting');
            const api = new CategoryApi();
            const categoryCreationDTO = new CategoryCreationDTO();
            categoryCreationDTO.name = name;
            api.createCategory({ categoryCreationDTO }, (e, d, r) => {
                if (e) {
                    StateHelper.simpleErrorManagement(e, 'mutationPostCategoryError', commit);
                } else {
                    commit('mutationPostCategorySuccess', d);
                }
            });
        },
        actionPutCategory({ commit }, { id, oldName, newName }) {
            commit('mutationPutCategoryWaiting');
            const api = new CategoryApi();
            const categoryModificationDTO = new CategoryModificationDTO();
            categoryModificationDTO.id = id;
            categoryModificationDTO.name = newName;
            api.updateCategory(oldName, { categoryModificationDTO }, (e, d, r) => {
                if (e) {
                    StateHelper.simpleErrorManagement(e, 'mutationPutCategoryError', commit);
                } else {
                    commit('mutationPutCategorySuccess', d);
                }
            });
        },
        actionDeleteCategory({ commit }, name) {// TODO add on server side first
            commit('mutationDeleteCategoryWaiting');
            const api = new CategoryApi();
            const categoryCreationDTO = new CategoryCreationDTO();
            categoryCreationDTO.name = name;
            api.deleteCategory({ categoryCreationDTO }, (e, d, r) => {
                if (e) {
                    StateHelper.simpleErrorManagement(e, 'mutationDeleteCategoryError', commit);
                } else {
                    commit('mutationDeleteCategorySuccess', d);
                }
            });
        },
    },
    mutations: {
        /**
         *
         * @param {*} state the application state in Vuex store to modify.
         * @param {*} data Additional data to update the state accordingly.
         */
        // Get
        mutationGetCategoriesWaiting(state) {
            state.categories.state = States.WAITING;
        },
        mutationGetCategoriesSuccess(state, categories) {
            state.categories.state = States.SUCCESS;
            state.categories.data = [...categories];
        },
        mutationGetCategoriesError(state, data) {
            state.categories.state = States.ERROR;
            state.categories.message = data.message;
            Notifications.error("Category Loading Error", data.message);
        },

        // Post
        mutationPostCategoryWaiting(state) {
            state.categories.state = States.WAITING;
        },
        mutationPostCategorySuccess(state) {
            // Just refresh all the categories form the server
            store.dispatch('actionGetCategories');
        },
        mutationPostCategoryError(state, data) {
            state.categories.state = States.ERROR;
            state.categories.message = data.message;
            Notifications.error("Category Create Error", data.message);

        },

        // Put
        mutationPutCategoryWaiting(state) {
            state.categories.state = States.WAITING;
        },
        mutationPutCategorySuccess(state) {
            // Just refresh all the categories form the server
            store.dispatch('actionGetCategories');
        },
        mutationPutCategoryError(state, data) {
            state.categories.state = States.ERROR;
            state.categories.message = data.message;
            Notifications.error("Category Update Error", data.message);
        },

        // Delete
        mutationDeleteCategoryWaiting(state) {
            state.categories.state = States.WAITING;
        },
        mutationDeleteCategorySuccess(state) {
            // Just refresh all the categories form the server
            store.dispatch('actionGetCategories');
        },
        mutationDeleteCategoryError(state, data) {
            state.categories.state = States.ERROR;
            state.categories.message = data.message;
            Notifications.error("Category Delete Error", data.message);
        },
    },
}

export default categories;