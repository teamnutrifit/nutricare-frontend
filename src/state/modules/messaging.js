import States from '@/state/States'
import StateHelper from '@/state/StateHelper';
import MessageApi from '@/gen/api/MessageApi';
import UserApi from '@/gen/api/UserApi';
import Notifications from '../../util/Notifications';


const messaging = {
  state: () => ({
    messaging: {
      state: States.INIT,
      message: "string",
      data: {
        id: 0,
        username: "username",
        friends: [],
        messages: [],
      }
    }
  }),
  getters: {
    getMessaging(state) {
      return state.messaging;
    },
    getMessages(state) {
      return state.messaging.data.messages;
    },
    getFriends(state) {
      return state.messaging.data.friends;
    }
  },
  actions: {
    /**
     *
     * @param {*} { commit } the context contains a function called commit used to commit mutations.
     * @param {*} data additional data object for the action
     */
    actionGetFriends({ commit }) {
      commit('mutationGetFriendsWaiting', {});
      const api = new UserApi();

      api.getFriends((e, d, r) => {
        if (e) {
          StateHelper.simpleErrorManagement(e, 'mutationGetFriendsError', commit);
        } else {
          commit('mutationGetFriendsSuccess', { ...d });
        }
      })
    },
    actionGetMessagingTo({ commit }) {
      commit('mutationGetMessagingWaiting', {});
      const api = new MessageApi();

      api.getMessages((e, d, r) => {
        if (e) {
          StateHelper.simpleErrorManagement(e, 'mutationGetMessagingError', commit);
        } else {
          commit('mutationGetMessagingSuccess', { ...d });
        }
      })
    },
    actionGetMessagingFrom({ commit }) {
      commit('mutationGetMessagingWaiting', {});
      const api = new MessageApi();

      api.getMessagesSent((e, d, r) => {
        if (e) {
          StateHelper.simpleErrorManagement(e, 'mutationGetMessagingError', commit);
        } else {
          commit('mutationGetMessagingSuccess', { ...d });
        }
      })
    },
    actionSendMessage({ commit }, obj) {
      commit('mutationSendMessagingWaiting', {});
      const api = new MessageApi();

      api.sendMessageTo(obj.friendUsername, { messageCreationDTO : obj.messageCreationDTO }, (e, d, r) => {
        if (e) {
          StateHelper.simpleErrorManagement(e, 'mutationSendMessagingError', commit);
        } else {
          commit('mutationSendMessagingSuccess', { ...d });
        }
      })
    },
  },
  mutations: {
    /**
     *
     * @param {*} state the application state in Vuex store to modify.
     * @param {*} data Additional data to update the state accordingly.
     */
    // Get Friends
    mutationGetFriendsWaiting(state, data) {
      state.messaging.state = States.WAITING;
    },
    mutationGetFriendsSuccess(state, data) {
      state.messaging.state = States.SUCCESS;
      state.messaging.data.friends = { ...data };
      //Notifications.debug("Got Friends Successfully", "You can now send messages to them.");
    },
    mutationGetFriendsError(state, data) {
      state.messaging.state = States.ERROR;
      state.messaging.message = data.message;
      Notifications.error("Erreur de Chargement", "On n'a pas pu charger la liste d'amis.");
    },

    // Get Messages
    mutationGetMessagingWaiting(state, data) {
      state.messaging.state = States.WAITING;
    },
    mutationGetMessagingSuccess(state, data) {
      state.messaging.state = States.SUCCESS;
      state.messaging.data.messages = { ...data };
      //Notifications.debug("Got Messages Successfully", "You can now see all your messages.");
    },
    mutationGetMessagingError(state, data) {
      state.messaging.state = States.ERROR;
      state.messaging.message = data.message;
      Notifications.error("Erreur de Chargement", "On n'a pas pu charger vos messages.");
    },

    // Send Message
    mutationSendMessagingWaiting(state, data) {
      state.messaging.state = States.WAITING;
    },
    mutationSendMessagingSuccess(state, data) {
      state.messaging.state = States.SUCCESS;
      Notifications.success("Envoi réussi", "Votre message a été envoyé.");
    },
    mutationSendMessagingError(state, data) {
      state.messaging.state = States.ERROR;
      state.messaging.message = data.message;
      Notifications.error("Erreur d'Envoi", "Votre message n'a pas pu être envoyé.");
    },
  },
}

export default messaging;
