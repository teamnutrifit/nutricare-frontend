import States from '@/state/States'
import router from '@/router/router'
import Notifications from '../../util/Notifications';
import StateHelper from '@/state/StateHelper';
import WaterApi from '@/gen/api/WaterApi'
import NutritionApi from '@/gen/api/NutritionApi'
import Utils from "@/util/Utils";

function initialState() {
  return {
    water: {
      state: States.INIT,
      message: "Default error message string",
      data: {
        // "id": 0,
        // "quantity": 0,
        // "dateOfComspution": "2021-01-10",
        // "user": {
        //   "username": "string",
        //   "firstname": "string",
        //   "lastname": "string",
        //   "birthdate": "2021-01-10",
        //   "sex": "string",
        //   "email": "string"
        // }
      }
    },
    waters: {
      state: States.INIT,
      message: "Default error message string",
      data: []
    },

    meals: {
      state: States.INIT,
      message: "Default error message string",
      data: [],
    },
    lastWeekMeals: { // Kcal of the last n Days used for the Week Widget
      state: States.INIT,
      message: "Default error message string",
      data: [],
    }
  }
}

let lastDateUsed = "";

const journal = {
  state: () => (initialState()),
  getters: {
    getWaterData(state) {
      return state.water.data;
    },
    getMealsData(state) {
      return state.meals.data;
    },
    getLastWeekMeals(state) {
      return state.lastWeekMeals.data;
    }
  },
  actions: {
    actionWaterGet({ commit }, date) {
      // console.log(date);

      commit('mutationWaterGetWaiting');
      const api = new WaterApi();

      api.getUserWaterDay(date, (e, d, r) => {
        if (e) {
          StateHelper.simpleErrorManagement(e, 'mutationWaterGetError', commit);
        } else {
          commit('mutationWaterGetSuccess', d);
        }
      });
    },

    actionWaterPut({ commit, dispatch }, waterModificationDTO) {
      // console.log(waterModificationDTO)
      commit('mutationWaterPutWaiting');
      const api = new WaterApi();

      api.modifyWater({ waterModificationDTO }, (e, d, r) => {
        if (e) {
          StateHelper.simpleErrorManagement(e, 'mutationWaterPutError', commit);
        } else {
          commit('mutationWaterPutSuccess', d);
          dispatch('actionWaterGet', waterModificationDTO.dateOfComspution);
        }
      });
    },

    actionMealsGet({ commit }, date) {
      // console.log(date);
      lastDateUsed = date;
      commit('mutationMealsGetWaiting');
      const api = new NutritionApi();

      api.getMealsDate(date, (e, d, r) => {
        if (e) {
          StateHelper.simpleErrorManagement(e, 'mutationMealsGetError', commit);
        } else {
          commit('mutationMealsGetSuccess', d);
        }
      });
    },

    actionMealPost({ commit, dispatch }, mealCreationDTO) {
      commit('mutationMealPostWaiting');
      const api = new NutritionApi();

      api.createMeal({ mealCreationDTO }, (e, d, r) => {
        if (e) {
          StateHelper.simpleErrorManagement(e, 'mutationMealPostError', commit);
        } else {
          commit('mutationMealPostSuccess', d);
          dispatch('actionMealsGet', lastDateUsed);
        }
      });
    },

    actionMealPutAddFoodIn({ commit, dispatch }, { mealId, foodInCreationDTO }) {
      // console.log(mealId)
      // console.log(foodInCreationDTO)
      commit('mutationMealPutAddFoodInWaiting');
      const api = new NutritionApi();

      api.addFood(mealId, { foodInCreationDTO }, (e, d, r) => {
        if (e) {
          StateHelper.simpleErrorManagement(e, 'mutationMealPutAddFoodInError', commit);
        } else {
          commit('mutationMealPutAddFoodInSuccess', d);
          dispatch('actionMealsGet', lastDateUsed);
        }
      });
    },

    actionMealPut({ commit, dispatch }, { mealId, mealCreationDTO }) {
      // console.log(mealId)
      // console.log(mealCreationDTO)
      commit('mutationMealPutWaiting');
      const api = new NutritionApi();

      api.updateMeal(mealId, { mealCreationDTO }, (e, d, r) => {
        if (e) {
          StateHelper.simpleErrorManagement(e, 'mutationMealPutError', commit);
        } else {
          commit('mutationMealPutSuccess', d);
          dispatch('actionMealsGet', lastDateUsed);
        }
      });
    },

    actionMealDelete({ commit, dispatch }, mealId) {
      // console.log(mealId)
      // console.log(foodInCreationDTO)
      commit('mutationMealDeleteWaiting');
      const api = new NutritionApi();

      api.deleteMeal(mealId, (e, d, r) => {
        if (e) {
          StateHelper.simpleErrorManagement(e, 'mutationMealDeleteError', commit);
        } else {
          commit('mutationMealDeleteSuccess', d);
          dispatch('actionMealsGet', lastDateUsed);
        }
      });
    },
    actionLastWeekMealsGet({ commit }, numberOfDays) {
      let arr = Utils.generateLastNDaysBackendDateArray(numberOfDays);
      // console.log(arr);

      commit('mutationLastWeekMealsGetWaiting');
      const api = new NutritionApi();

      let lastWeekMeals = [];

      for (let i = 0; i < arr.length; i++) {
        const date = arr[i];

        api.getMealsDate(date, (e, d, r) => {
          if (e) {
            StateHelper.simpleErrorManagement(e, 'mutationLastWeekMealsGetError', commit);
          } else {
            let totalKcalOfTheDay = 0;
            for (let j = 0; j < d.length; j++) {
              const meal = d[j];
              totalKcalOfTheDay += Utils.methodComputeFoodsValuesTotal(meal.foods).kcal;
            }
            commit('mutationLastWeekMealsGetSuccess', { x: date, y: totalKcalOfTheDay });
          }
        });
      }

    },
  },
  mutations: {
    // Get
    mutationWaterGetWaiting(state) {
      state.water.state = States.WAITING;
    },
    mutationWaterGetSuccess(state, data) {
      state.water.state = States.SUCCESS;
      state.water.data = data;
      Notifications.debug("Water Loading", "Successful.");
    },
    mutationWaterGetError(state, data) {
      state.water.state = States.ERROR;
      state.water.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Put
    mutationWaterPutWaiting(state) {
      state.water.state = States.WAITING;
    },
    mutationWaterPutSuccess(state, data) {
      state.water.state = States.SUCCESS;
      // The data is only a message.
      // state.water.data = data;
      Notifications.debug("Modification Réussie", "La consommation d'eau a été mise à jour.");
    },
    mutationWaterPutError(state, data) {
      state.water.state = States.ERROR;
      state.water.message = data.message;
      Notifications.error("Erreur de Modification", data.message);
    },

    // MEAL
    // Get
    mutationMealsGetWaiting(state) {
      state.meals.state = States.WAITING;
    },
    mutationMealsGetSuccess(state, data) {
      state.meals.state = States.SUCCESS;
      state.meals.data = data;
      Notifications.debug("Meals Loading", "Successful.");
    },
    mutationMealsGetError(state, data) {
      state.meals.state = States.ERROR;
      state.meals.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },

    // Post
    mutationMealPostWaiting(state) {
      state.meals.state = States.WAITING;
    },
    mutationMealPostSuccess(state, data) {
      state.meals.state = States.SUCCESS;
      Notifications.success("Ajout Réussi", "Le repas a été crée.");
    },
    mutationMealPostError(state, data) {
      state.meals.state = States.ERROR;
      state.meals.message = data.message;
      Notifications.error("Erreur d'Ajout", data.message);
    },

    // Put
    mutationMealPutAddFoodInWaiting(state) {
      state.meals.state = States.WAITING;
    },
    mutationMealPutAddFoodInSuccess(state, data) {
      state.meals.state = States.SUCCESS;
      Notifications.debug("Ajout Réussi", "L'aliment a été ajouté.");
    },
    mutationMealPutAddFoodInError(state, data) {
      state.meals.state = States.ERROR;
      state.meals.message = data.message;
      Notifications.error("Erreur d'Ajout", data.message);
    },

    mutationMealPutWaiting(state) {
      state.meals.state = States.WAITING;
    },
    mutationMealPutSuccess(state, data) {
      state.meals.state = States.SUCCESS;
      Notifications.debug("Modification", "Le repas a été modifié.");
    },
    mutationMealPutError(state, data) {
      state.meals.state = States.ERROR;
      state.meals.message = data.message;
      Notifications.error("Erreur de Modification", data.message);
    },

    // Delete
    mutationMealDeleteWaiting(state) {
      state.meals.state = States.WAITING;
    },
    mutationMealDeleteSuccess(state, data) {
      state.meals.state = States.SUCCESS;
      Notifications.success("Suppression Réussie", "Le repas a été supprimé.");
    },
    mutationMealDeleteError(state, data) {
      state.meals.state = States.ERROR;
      state.meals.message = data.message;
      Notifications.error("Erreur de Suppression", data.message);
    },

    // LastWeekMeals
    // Get
    mutationLastWeekMealsGetWaiting(state) {
      state.lastWeekMeals.state = States.WAITING;
      Notifications.debug("LastWeekMeals Waiting", "Started.");
      state.lastWeekMeals.data = [];
    },
    mutationLastWeekMealsGetSuccess(state, data) {
      state.lastWeekMeals.state = States.SUCCESS;
      state.lastWeekMeals.data.push(data);
      // Notifications.debug("LastWeekMeals Loading", "Successful.");
    },
    mutationLastWeekMealsGetError(state, data) {
      state.lastWeekMeals.state = States.ERROR;
      state.lastWeekMeals.message = data.message;
      Notifications.error("Erreur de Chargement", data.message);
    },
  },
}

export default journal;
