import States from '@/state/States';
import WeightApi from '@/gen/api/WeightApi';

const weight = {
    state: () => ({
      weight: {
            state: States.INIT,
            message: "message",
            data: {
                "id": 0,
                "weight": 42.42,
                "weightDate": "27-12-2012",
            }
        },
    }),
    getters: {
        getWeight(state) {
            return state.weight;
        },
    },
    actions: {
        /**
         *
         * @param {*} { commit } the context contains a function called commit used to commit mutations.
         * @param {*} weightCreationDTO additional data object for the action
         */
        actionCreateWeight({ commit }, weightCreationDTO) {
          // First commit the waiting mutation
          commit('mutationWeightWaiting', {});
          const api = new WeightApi();

          api.createWeight({ weightCreationDTO }, (e, d, r) => {
            if (e) {
              commit('mutationWeightError', { ...e.response.body });
            } else {
              commit('mutationWeightSuccess', d);
            }
          });
        },
    },
    mutations: {
        /**
         *
         * @param {*} state the application state in Vuex store to modify.
         */
        mutationWeightWaiting(state) {
            state.weight.state = States.WAITING;
        },
        mutationWeightSuccess(state, weight) {
            state.weight.state = States.SUCCESS;
            state.weight.data = {...weight};
        },
        mutationWeightError(state, data) {
            state.weight.state = States.ERROR;
            state.weight.message = data.message;
        },
    },
}

export default weight;
