import States from '@/state/States'
import router from '@/router/router'
import Notifications from '../../util/Notifications';
import StateHelper from '@/state/StateHelper';
import NutritionApi from '@/gen/api/NutritionApi'

function initialState() {
    return {
        state: States.INIT,
        message: "Default error message string",
        data: [
            // {
            //     "id": 0,
            //     "name": "Poulet",
            //     "description": "Aliment ultime",
            //     "carbohydrate": 0,
            //     "sugar": 0,
            //     "fat": 0,
            //     "saturated": 0,
            //     "protein": 0,
            //     "kcal": 0 // We dont use this we recompute
            // },
            // {
            //     "id": 1,
            //     "name": "string",
            //     "description": "string",
            //     "carbohydrate": 0,
            //     "sugar": 0,
            //     "fat": 0,
            //     "saturated": 0,
            //     "protein": 0,
            //     "kcal": 0
            // }
        ],
        modal: {
            state: States.INIT,
            message: "Default error message",
            modalIsVisible: false,
            data: {

            }
        }
    }
}

let LastSearchString = "";

const food = {
    state: () => (initialState()),
    getters: {
        getFood(state) {
            return state;
        },
        getFoodModal(state) {
            return state.modal;
        }
    },
    actions: {
        actionFoodGet({ commit }, searchString) {
            LastSearchString = searchString;
            commit('mutationFoodGetWaiting');
            const api = new NutritionApi();

            if (searchString === undefined || searchString === "") {
                api.getFood((e, d, r) => {
                    if (e) {
                        StateHelper.simpleErrorManagement(e, 'mutationFoodGetError', commit);
                    } else {
                        commit('mutationFoodGetSuccess', d);
                    }
                });
            } else {
                api.searchFood(searchString, (e, d, r) => {
                    if (e) {
                        StateHelper.simpleErrorManagement(e, 'mutationFoodGetError', commit);
                    } else {
                        commit('mutationFoodGetSuccess', d);
                    }
                });
            }

        },
        actionFoodPost({ commit, dispatch }, foodCreationDTO) {
            commit('mutationFoodPostWaiting');
            const api = new NutritionApi();

            api.createFood({ foodCreationDTO }, (e, d, r) => {
                if (e) {
                    StateHelper.simpleErrorManagement(e, 'mutationFoodPostError', commit);
                } else {
                    commit('mutationFoodPostSuccess', d);
                    dispatch("actionFoodHideModal");
                    dispatch('actionFoodGet', LastSearchString);
                }
            });
        },

        actionFoodPut({ commit, dispatch }, foodModificationDTO) {
            commit('mutationFoodPutWaiting');
            const api = new NutritionApi();

            api.updateFood({ foodModificationDTO }, (e, d, r) => {
                if (e) {
                    StateHelper.simpleErrorManagement(e, 'mutationFoodPutError', commit);
                } else {
                    commit('mutationFoodPutSuccess', d);
                    dispatch("actionFoodHideModal");
                    dispatch('actionFoodGet', LastSearchString);
                }
            });
        },

        // Modal
        actionFoodShowModal({ commit }) {
            commit('mutationFoodShowModal');

        },
        actionFoodHideModal({ commit }) {
            commit('mutationFoodHideModal');

        },
    },
    mutations: {
        // Get
        mutationFoodGetWaiting(state) {
            state.state = States.WAITING;
        },
        mutationFoodGetSuccess(state, data) {
            state.state = States.SUCCESS;
            state.data = [...data];
            Notifications.debug("Food Get Successful", "Browse the list of foods");
        },
        mutationFoodGetError(state, data) {
            state.state = States.ERROR;
            state.message = data.message;
            Notifications.error("Erreur de Chargement", data.message);
        },

        // Post
        mutationFoodPostWaiting(state) {
            state.state = States.WAITING;
        },
        mutationFoodPostSuccess(state, data) {
            state.state = States.SUCCESS;
            Notifications.success("Ajout Réussi", "L'aliment a été crée.");
        },
        mutationFoodPostError(state, data) {
            state.state = States.ERROR;
            state.message = data.message;
            Notifications.error("Erreur d'Ajout", data.message);
        },

        // Put
        mutationFoodPutWaiting(state) {
            state.state = States.WAITING;
        },
        mutationFoodPutSuccess(state, data) {
            state.state = States.SUCCESS;
            Notifications.success("Modification Réussie", "L'aliment a été mis à jour.");
        },
        mutationFoodPutError(state, data) {
            state.state = States.ERROR;
            state.message = data.message;
            Notifications.error("Erreur de Modification", data.message);
        },

        // Modal
        mutationFoodShowModal(state) {
            state.modal.modalIsVisible = true;
        },
        mutationFoodHideModal(state) {
            state.modal.modalIsVisible = false;
            state.modal.data = initialState().modal.data;
        },
    },
}

export default food;
